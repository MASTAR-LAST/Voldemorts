# Voldemorts
Voldemorts, It is a powerful tool capable of encrypting files in various formats with base64 encoding with a salt with a default value of 16 bytes that can be changed and a password to encrypt and decrypt these files.

# Supported systems
| System      | Supporting |
| ----------- | :-----------: |
| Windows      | Not Supported      |
| Linux   | Supported        |
| Mac   | Not Sure Yet        |

## Installation
Install the tool with git

```bash
git clone https://github.com/MASTAR-LAST/Voldemorts.git && cd Voldemorts && sudo chmod u+x voldemorts.sh && ./voldemorts.sh
```

### Using instructions

```zsh
sudo voldemorts.py [folder name] --encrypt --salt-size 128
```
## Note
**use this tool in super user mood.**
![Screenshot](https://github.com/MASTAR-LAST/Voldemorts/assets/79379000/52294bc0-9f3e-4a89-94e0-bf4d57932594)
